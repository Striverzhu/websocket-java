package server;

import javax.websocket.Session;
import java.io.IOException;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * Created by xingyuzhu on 2017/10/21.
 */

public class MessageData {
    //单例
    private static MessageData instance;

    //主显示界面socket
    private Session mainSession;

    //concurrent包的线程安全Set，用来存放每个客户端对应的MyWebSocket对象。若要实现服务端与单一客户端通信的话，可以使用Map来存放，其中Key可以为用户标识
    private CopyOnWriteArraySet<Session> webSocketSet;

    private MessageData() {
    }

    public static MessageData getInstance() {
        if (instance == null) {
            instance = new MessageData();
        }
        return instance;
    }

    public void sendMainMessage(String message) {
        try {
            if (mainSession != null) {
                mainSession.getBasicRemote().sendText(message);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void sendMessage(String message) {
        for (Session item : webSocketSet) {
            try {
                item.getBasicRemote().sendText(message);
            } catch (IOException e) {
                e.printStackTrace();
                continue;
            }
        }
    }

    public void setMainSession(Session mainSession) {
        this.mainSession = mainSession;
    }

    public CopyOnWriteArraySet<Session> getWebSocketSet() {
        if (webSocketSet == null) {
            webSocketSet = new CopyOnWriteArraySet<Session>();
        }
        return webSocketSet;
    }
}
