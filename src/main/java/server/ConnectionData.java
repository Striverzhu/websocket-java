package server;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;

/**
 * Created by xingyuzhu on 2017/10/21.
 */
@ServerEndpoint("/connectdata")
public class ConnectionData {
    //消息管理器
    private MessageData messageData = MessageData.getInstance();

    /**
     * 连接建立成功调用的方法
     *
     * @param session 可选的参数。session为与某个客户端的连接会话，需要通过它来给客户端发送数据
     */
    @OnOpen
    public void onOpen(Session session) {
        //将主界面socket注册到消息管理器
        messageData.setMainSession(session);
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose() {
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @param message 客户端发送过来的消息
     * @param session 可选的参数
     */
    @OnMessage
    public void onMessage(String message, Session session) {
    }

    /**
     * 发生错误时调用
     *
     * @param session
     * @param error
     */
    @OnError
    public void onError(Session session, Throwable error) {
        System.out.println("发生错误");
        error.printStackTrace();
    }
}
