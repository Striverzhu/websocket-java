<html>
<head>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
<h2>Hello World!</h2>
<input id="text" type="text"/>
<button onclick="send()">send message.</button>
<hr/>
<button onclick="closeWebSocket()">close websocket</button>
<hr/>
<div id="message"></div>
</body>
<script type="text/javascript">
  var websocket = null;
  if ('WebSocket' in window) {
    websocket = new WebSocket("ws://localhost:8080/websocket");
  } else {
    alert("websocket not supported.");
  }
  websocket.onerror = function () {
    setMessageInnnerHTML("websocket connect error.");
  }

  websocket.onopen = function () {
    setMessageInnnerHTML("websocket connect success.");
  }

  websocket.onmessage = function (event) {
    setMessageInnnerHTML(event.data);
  }

  websocket.onclose = function () {
    setMessageInnnerHTML("websocket connect close.");
  }

  //在关闭页面之前监听事件，将websocket关闭
  window.onbeforeunload = function () {
    closeWebSocket();
  }

  function closeWebSocket() {
    websocket.close();
  }

  function send() {
    websocket.send(document.getElementById('text').value);
  }

  function setMessageInnnerHTML(message) {
    document.getElementById('message').innerHTML += message + '<br/>';
  }
</script>
</html>
