<%--
  Created by IntelliJ IDEA.
  User: xingyuzhu
  Date: 2017/10/21
  Time: 17:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Connect</title>
</head>
<body>
<div id="message"></div>
</body>
<script type="text/javascript">
  var websocket = null;
  if ('WebSocket' in window) {
    websocket = new WebSocket("ws://localhost:8080/connectdata");
  } else {
    alert("websocket not supported.");
  }
  websocket.onerror = function () {
    setMessageInnnerHTML("websocket connect error.");
  }

  websocket.onopen = function () {
    setMessageInnnerHTML("websocket connect success.");
  }

  websocket.onmessage = function (event) {
    setMessageInnnerHTML(event.data);
  }

  websocket.onclose = function () {
    setMessageInnnerHTML("websocket connect close.");
  }

  //在关闭页面之前监听事件，将websocket关闭
  window.onbeforeunload = function () {
    closeWebSocket();
  }

  function closeWebSocket() {
    websocket.close();
  }

  function setMessageInnnerHTML(message) {
    document.getElementById('message').innerHTML += message + '<br/>';
  }
</script>
</html>
